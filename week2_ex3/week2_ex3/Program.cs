﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week2_ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = 0;
            var choice = 0;
            const double m2km = 1.609344;
            const double km2m = 0.621371;

            Console.WriteLine("Please enter 1 for km-mile conversion or 2 for miles-kms:  ");
            choice = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter number of miles / kms:  ");
            input = int.Parse(Console.ReadLine());            

            switch (choice)
            {
                case 1:
                    Console.WriteLine($"{input} kms = {input * km2m} miles");
                    break;
                case 2:
                    Console.WriteLine($"{input} miles = {input * m2km} kms");
                    break;
                default:
                    Console.WriteLine("invalid choice");
                    break;
            }
            
        }
    }
}
