﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise02
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = Console.ReadLine();

            switch (colour)
            {
                case "blue":
                    Console.WriteLine("You chose blue - The Sky is Blue");
                    break;
                case "red":
                    Console.WriteLine("You chose Red - Roses are red");
                    break;
                default:
                    Console.WriteLine("Invalid colour");
                    break;
            }

        }
    }
}
